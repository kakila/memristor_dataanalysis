## Extract slopes of IV curves
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-10-31

## Dependencies
# The script assumes that the data was parsed and _filenames_ and _*input_ 
# variables exist
pkg load gpml

## Extract slopes
#
if ~exist('s', 'var');
  s = zeros (nfiles, 1); 
  for i = 1:nfiles
    d    = load (filenames{i}); 
    s(i) = cov (d(:,1), d(:,2));
  endfor
endif

## Approximate surface
#
lf = log10 (Freqinput);
a  = Amplinput;

if ~exist('hyp', 'var')
  args    = {@infExact, [], @covSEard, @likGauss, [lf a], s};
  hyp.cov = [0.5; -1.2; -2]; hyp.lik = -4;
  hyp     = minimize_minfunc (hyp, @gp, -100, args{:});
endif

if ~exist('S', 'var')
  lfi    = linspace (0, max (lf), 30); 
  ai     = linspace (0.05, 0.5, 20) ;
  [LF A] = meshgrid (lfi, ai);
  S      = reshape (gp (hyp, args{:}, [LF(:) A(:)]), size (LF));
endif

figure (1); clf
  plot3 (lf, a, s, '.r', 'markersize', 15);
  axis tight
  ylim([0.05 0.5]);
  hold on
  surf (LF, A, S, 'edgecolor', 'none', 'facealpha', 0.7);
  hold off
  xlabel ('log10 Freq')
  ylabel ('Amplitude')
  zlabel ('IV Slope')
  view (-34, 36);
  shading interp
