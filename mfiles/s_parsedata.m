## Parse raw data into GNU Octave format
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-10-31

filenames = glob ('../data/raw/*.txt');
nfiles = numel (filenames);

## Parse input data
# The header of the files contains amplitude of the input signal (trinagular waves)
# in volts and the frequency.
if !exist ('Freqinput', 'var')
  Freqinput = Vppinput = Ippinput = zeros (nfiles, 1);
  tic
  toremove = [];
  for i=1:nfiles
    fid = fopen (filenames{i}, 'r');
    Vppinput(i) = str2num (strsplit (fgetl (fid), ':'){2});
    Freqinput(i) = str2num (strsplit (fgetl (fid), ':'){2});
    fclose (fid);
    if Vppinput(i) < sqrt (eps)
      toremove = [toremove, i];
    endif
  endfor
  Vppinput(toremove) = [];
  Freqinput(toremove) = [];
  filenames(toremove) = [];
  nfiles = numel (filenames);
  toc
endif
printf ('Removed file id: '); printf ('%d ', toremove); printf ('\n');
fflush (stdout);
% variables to save
vars = {'Vppinput', 'Freqinput', 'filenames'};

## Compute summary values
# Measure nonlinearity as deviation from the line
#
function [s err imax] = lindev (fname)
  xy   = load (fname);

  % Maximum absolute output
  imax = max (abs (xy(:,2)));

  xy = zscore (xy);

  s = svd (xy, 1);
  s = s(2) / s(1);

  A   = vander (xy(:,1), 2);
  err = max ( abs (xy(:,2) - A * (A \ xy(:,2))));
endfunction

pkg load parallel
tic
[ldev, mae, Imaxabs] = parcellfun (nproc, @lindev, filenames);
toc
vars = {vars{:}, 'ldev', 'mae', 'Imaxabs'};

ofile = '../data/data_summary.mat';
save (ofile, '-v7', vars{:});
printf ('Parameters saved to %s\n', ofile);
