## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-11-16

function [V I Amp Freq ldev mae] = load_datarange (a=[], f=[], ind=[])
  persistent A F fname LDEV MAE
  if isempty (A)
    ifile = '../data/data_summary.mat';
    load (ifile);
    A = Vppinput;
    F = Freqinput;
    fname = filenames;
    LDEV = ldev;
    MAE = mae;
    IMX = Imaxabs;

    clear Vppinput Freqinput filenames ldev mae Imaxabs
  endif
  if nargin == 0
    V = struct ('Amp', 'Freq', 'ldev', 'mae');
    V.Amp  = A;
    V.Freq = F;
    V.ldev = LDEV;
    V.mae  = MAE;
    V.Imax = IMX;
    return
  endif

  tf = true (numel (fname), 1);
  if !isempty (a)
    tf = tf & ((A >= a(1)) & (A <= a(2))); % select amplitudes
  endif
  if !isempty (f)
    tf = tf & ((F >= f(1)) & (F <= f(2))); % select frequencies
  endif

  IND = find (tf);
  if !isempty (ind)
    i = ismember (ind, IND); % use only subset of indexes
    ind = ind(i);
  else
    ind = IND;
  endif

  % indexes
  nf = length (ind);
  V  = I = cell (nf, 1);
  for i = 1:nf
    d    = load (fname{ind(i)});
    V{i} = d(:,1);
    I{i} = d(:,2);
  endfor
  Amp  = A(ind);
  Freq = F(ind);
  ldev = LDEV(ind);
  mae  = MAE(ind);
endfunction
