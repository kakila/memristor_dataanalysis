#Won't work while the OSF project is private
#wget https://osf.io/52bfd/download

# Expand into raw folder
unzip -jao dataset_allraw.zip -d raw

# Remove spaces and parenthesies from filenames
# Commet first 3 lines (header) using #
cd raw
IFS="\n"
for file in *.txt;
do
    nfile=${file//[[:space:]()]}
    mv -f "$file" "$nfile"
    sed -i -e '1,3 s/^/#/' "$nfile"
done

